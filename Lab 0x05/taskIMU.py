"""!@file       taskIMU.py
    @brief      Runs tasks sent by the taskUser file and keeps the IMU measurement data updated.
    @details    Acts as a background function file controlling the IMU for the primary user interface
                so that the user interface does not have to perform IMU updating directly. It will provide the euler
                angle and angular velocity as a shared variable for use in other files and can calibrate the IMU
                when told to by the user task. It will continuously update the IMU every 10 milliseconds when run from
                main.

                \n The State Transition Diagram for taskIMU can be seen here.
                @image html IMUFSM.PNG width=1000px
                @image html IMUFSMIO.PNG width=500px

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x05/taskIMU.py
    @author     Lucas Murray
    @author     Max Cassady
    @date       March 3, 2022
"""

#import pyb
from time import ticks_us, ticks_add, ticks_diff
import micropython
from I2C import BNO055

## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Update variable as the constant 1
#
S1_UPDATE = micropython.const(1)

## @brief Defines the State 2 Calibration variable as the constant 2
#
S2_CAL = micropython.const(2)


def taskIMUFcn(taskName, period, cFlag, duty, Eang, Vang, Cast):
    '''!@brief              Controls the IMU as determined by the taskUser file
        @details            Interfaces with the taskUser file through the use of
                            the shared flag and tuple parameters, keeping the IMU
                            updated and performing calibration when told to.
        @param taskName     The name of the task a string
        @param period       The period of the task in microseconds as an integer
        @param cFlag        Boolean variable shared between taskUser and
                            taskIMU for the IMU calibration command
        @param duty         Tuple data shared between taskUser and
                            taskEncoder for the motor duty
        @param Eang         Tuple shared between taskUser and
                            taskEncoder that stores the euler angle data from the IMU
        @param Vang         Tuple shared between taskUser and
                            taskEncoder that stores the angular velocity data from the IMU
        @param Cast         Tuple shared between taskUser and
                            taskEncoder that would have allowed for the automated calibration of the IMU
    '''

    state = S0_INIT

    next_time = ticks_add(ticks_us(), period)
    
    driver = BNO055()
    driver.opMode(driver.NDOF)

    while True:
        current_time = ticks_us()                       # Keeps updating current time
        
        if ticks_diff(current_time, next_time) >= 0:    # Waits for current_time to pass next_time
            
            if state == S0_INIT:
                state = S1_UPDATE
                

            # ---------- Update Euler Angles/Velocities ----------
            elif state == S1_UPDATE:

                Eang.write(driver.getEulerAngles()) # Euler Angles
                Vang.write(driver.getVelocity())    # Euler Rates
                
                if cFlag.read() == True:
                    state = S2_CAL
                    

            # -------------- Calibrate Touch Panel --------------
            elif state == S2_CAL:
                driver.getCalCoefficients(cFlag, Cast)
                state = S1_UPDATE
            

            
            else:
                raise ValueError(f'Invalid State in {taskName}')
                
            next_time = ticks_add(next_time, period)
            
            yield state
            
        else:
            yield None
