"""!@file       I2C.py
    @brief      A driver that runs and controls a BNO055 IMU.
    @details    The driver class instantiates a driver that can instantiate and control an BNO055 Inertial Measurement
                Unit (IMU) and calibrate it. The driver uses the pyb library functionality for I2C serial communication
                ports to read the calibration status and either set the coefficients from an already written file or
                write a new calibration file. The I2C class will update the euler angles and angular velocity read
                and converted from the relevant IMU registers.

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x05/I2C.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       March 3, 2022
"""


#from motor import Motor
from pyb import I2C
import os

class BNO055:
    '''!@brief   A BNO055 IMU object that communicates with a microcontroller through an I2C
        @details Objects of this class can be calibrated and are able to update their euler angle and angular velocity
                 outputs. They can also read and write to files, skipping the calibration process if the calibration
                 file already exists and writing a new one if it doesn't.
    '''

    def __init__(self):
        '''!@brief Initializes an I2C object from the pyb library.
        '''
        self.i2c = I2C(1, I2C.CONTROLLER, baudrate = 100000)
        self.IMU = 0b00001000
        self.COMPASS = 0b00001001
        self.M4G = 0b00001010
        self.NDOF_FMC_OFF = 0b00001011
        self.NDOF = 0b00001100
        self.CONFIG_MODE = 0b00000000
    
    
    
    def opMode(self, fusionMode):
        '''!@brief Changes the operating mode of the I2C to a given fusion mode
            @param fusionMode  The fusion mode that the user wants to switch the I2C to
        '''
        self.i2c.mem_write(fusionMode, 0x28, 0x3D, timeout = 20, addr_size = 8)
    
    
    
    def getCalStatus(self):
        '''!@brief Reads and returns the current calibration status of the IMU
        '''
        calStatus = self.i2c.mem_read(1, 0x28, 0x35)[0]
        
        sysStatus = (calStatus & 0b11000000) >> 6
        gyrStatus = (calStatus & 0b00110000) >> 4
        accStatus = (calStatus & 0b00001100) >> 2
        magStatus = calStatus & 0b00000011
        
        return [sysStatus, gyrStatus, accStatus, magStatus]
    
    
    
    def getCalCoefficients(self, cFlag, Cast):
        '''!@brief Sets the calibration coefficients of the IMU from a file or from manual calibration
            @details If the calibration file already exists, it will read it and set the calibration coefficients
                     of the IMU from the read data, and if the file doesn't exist, it runs the calibration process
                     and sends the new IMU coefficients over to setCalCoefficients().
            @param cFlag A shared variable that tells the UI when the calibration is finished
            @param Cast A currently unused shared variable that would allow an automated calibration to work
        '''
        file = "IMU_cal_coeffs.txt"
        if file in os.listdir():

            with open(file, 'r') as f:

                calDataString = f.readline()
                calValues = [str(calValue) for calValue in calDataString.strip().split(',')]
                calValues = ''.join(calValues)

                self.i2c.mem_write(calValues, 0x28, 0x55)
                print('Calibration file read')
                cFlag.write(False)
        else:
            print('Calibration file does not exist')
            
            while True:
                self.opMode(self.NDOF)
                s,g,a,m = self.getCalStatus()
                print(self.getCalStatus())

                if s==3 and g==3 and a==3 and m==3:
                    cFlag.write(False)
                    break
            
            self.setCalCoefficients()
            print('Creating new calibration file')
        
        
        
    def setCalCoefficients(self):
        '''!@brief Writes the newly obtained calibration coefficients of the IMU to a new file
            @details Only accessed when getCalCoefficients() doesn't find a calibration file, it will read the new
                     coefficients from the relevant IMU registers and write them to a new file with the required name.
        '''
        self.coefficients = self.i2c.mem_read(22, 0x28, 0x55)
        with open("IMU_cal_coeffs.txt", 'w') as f:
            (A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V) = self.coefficients
            lst = (A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V)
            string = ''

            for item in lst:
                a = hex(item)
                string += (a + ',')

            string = string[:-1]
            f.write(string)
        
        
    
    def getEulerAngles(self):
        '''!@brief Updates the data read from the IMU and converts it to Euler Angles (Pitch, Roll, Yaw)
            @details Reads the angle data from the relevant registers, then bitshifts half of them because the IMU
                     outputs the data in an LSB and MSB form which needs to be combined to be read. It then converts
                     the data into a readable degree format.
        '''
        angles = self.i2c.mem_read(6, 0x28, 0x1A)
            
        # ----------- Pitch Angle -----------
        pitch    = ((angles[5] << 8) | angles[4])
        if pitch > 32768:
            pitch -= 65535
        pitch /= 16
        
        # ----------- Roll Angle -----------
        roll     = ((angles[3] << 8) | angles[2])
        if roll > 32768:
            roll -= 65535
        roll /= 16 
        
        # ----------- Yaw Angle -----------
        yaw      = ((angles[1] << 8) | angles[0])
        yaw /= 16
        
        return(pitch, roll, yaw)
    
    
    
    def getVelocity(self):
        '''!@brief Updates the data read from the IMU and converts it to Angular Velocities
            @details Reads the anglular velocity data from the relevant registers, then bitshifts half of them because
                     the IMU outputs the data in an LSB and MSB form which needs to be combined to be read. It then
                     converts the data into a readable degree/second format.
        '''
        speeds = self.i2c.mem_read(6, 0x28, 0x14)
        
        # ----------- Pitch Rate -----------
        angularX = ((speeds[1] << 8) | speeds[0])
        if angularX > 32768:
            angularX -= 65535
        angularX /= -16
        
        # ------------ Roll Rate ------------
        angularY = ((speeds[3] << 8) | speeds[2])
        if angularY > 32768:
            angularY -= 65535
        angularY /= -16
        
        # ------------ Yaw Rate ------------
        angularZ = ((speeds[5] << 8) | speeds[4])
        if angularZ > 32768:
            angularZ -= 65535
        angularZ /= -16
        
        return(angularX, angularY, angularZ)
    
    
    
if __name__ == '__main__':
    # Driver test code
    driver = BNO055()
    driver.opMode(driver.NDOF)
    
    driver.getCalCoefficients(True)
    
    """
    while True:
        
        print('Euler Angles')
        print(driver.getEulerAngles())

        print('Angular Velocities')
        print(driver.getVelocity())
        '''
        print('Calibration Status')
        
        print(driver.getCalStatus())
        '''
        driver.getCalCoefficients()
        #driver.setCalCoefficients()
        
        
        file = open('IMU_cal_coeffs.txt', 'r')
        print(file.read())
        '''
        
        '''
        # Motor 1 pins
        pinB4 = Pin(Pin.cpu.B4, Pin.OUT_PP)
        pinB5 = Pin(Pin.cpu.B5, Pin.OUT_PP)
        
        # Motor 2 pins
        pinB0 = Pin(Pin.cpu.B0, Pin.OUT_PP)
        pinB1 = Pin(Pin.cpu.B1, Pin.OUT_PP)
        
        motor1 = Motor(pinB4, pinB5, 1, 2)
        motor2 = Motor(pinB0, pinB1, 3, 4)
        
        # Set the duty cycle of the first motor to 40 percent and the duty cycle of
        # the second motor to 60 percent
        #motor1.setDuty(40)
        #motor2.setDuty(25)
        """