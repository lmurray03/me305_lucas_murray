"""!@file       closedLoopControl.py
    @brief      A driver to provide a feedback loop for closed loop control.
    @details    The closed loop class instantiates a closed loop controller that will calculate the error of the
                current motor speed and compare to a reference speed, updating the motor speed based on the error
                calculated. The closed loop controller currently only calculates the error using proportional gain.

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x04/closedLoopControl.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       February 24, 2022
"""


import math

class ClosedLoop:
    '''!@brief A closed loop feedback object that calculates error in velocity.
        @details Objects of this class can find the error in the current velocity when compared to an arbitrary
                 desired velocity when paired with a proportional gain coefficient.
    '''

    def __init__(self, controlParameters, delta):
        '''!@brief Initializes and returns a closed loop object.
            @param controlParameters  A shared variable that contains the reference velocity and proportional gain
            @param delta  A shared variable that contains the change in position of the motor, updated every 10ms
        '''
        self.actualVel = delta * 0.05 * math.pi
        self.refVel, self.propGain = controlParameters.read()
    
    def update(self, controlParameters, delta):
        '''!@brief Updates the feedback error found by the closed loop control.
            @param controlParameters  A shared variable that contains the reference velocity and proportional gain
            @param delta  A shared variable that contains the change in position of the motor, updated every 10ms
        '''
        self.refVel, self.propGain = controlParameters.read()
        #self.refVel *= 2.17
        self.motorDuty = (-1 * self.propGain * (self.refVel - self.actualVel))
        self.actualVel = delta * 0.05 * math.pi
    
    def setGain(self):
        '''!@brief A currently unused function that would set the gain value for the proportional control.
            @details Instead of using this function, we found it easier to use a shared variable that contains the gain
                     as the gain is reference in the user Task which never instantiates a closed loop object
        '''
        pass