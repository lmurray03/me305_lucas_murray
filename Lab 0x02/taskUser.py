"""!@file       taskUser.py
    @brief      Runs a user interface for someone to run the encoder
    @details    Uses a function to accept user input and run the desired method.
                Uses the shared variables to read data written by the encoder and motor tasks and to send flags that
                will tell either of the functions to perform a certain action.

                \n The State Transition Diagram for taskUser can be seen here.
                @image html taskUser_FSM.PNG width=1000px
                @image html task_I_O_FSM.PNG width=500px
                @image html taskUser_O.PNG width=500px

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x02/taskUser.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       February 24, 2022
"""

from time import ticks_us, ticks_add, ticks_diff
from pyb import USB_VCP
import micropython
import array, math, gc

## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Command variable as the constant 1
#
S1_CMD = micropython.const(1)
## @brief Defines the State 2 Zero variable as the constant 2
#
S2_ZERO = micropython.const(2)

## @brief Defines the State 3 Print variable as the constant 3
#
S3_PRINT = micropython.const(3)

## @brief Defines the State 4 Delta variable as the constant 4
#
S4_DELTA = micropython.const(4)

## @brief Defines the State 5 Collect variable as the constant 5
#
S5_COLLECT = micropython.const(5)

## @brief Defines the State 6 Velocity variable as the constant 6
#
S6_VELOCITY = micropython.const(6)

## @brief Defines the State 7 Duty Cycle variable as the constant 7
#
S7_GETDUTY = micropython.const(7)

## @brief Defines the State 8 Test variable as the constant 8
#
S8_TEST = micropython.const(8)

## @brief Defines the State 9 Fault variable as the constant 9
#
S9_FAULT = micropython.const(9)

## @brief Defines the State 10 Reference Velocity variable as the constant 10
#
S10_REF = micropython.const(10)

## @brief Defines the State 11 Set Gain variable as the constant 11
#
S11_SETGAIN = micropython.const(11)

##@brief Defines the State 12 Closed Loop Control variable as the constant 12
#
S12_CLOOP = micropython.const(12)

##@brief Defines the State 14 Step Response Test variable as the constant 14
#
S14_STEP = micropython.const(14)

## @brief Defines the State 20 End variable as the constant 20
#
S20_END = micropython.const(20)


def taskUserFcn(taskName, period, zFlag, mFlag, MFlag, cFlag, wFlag, data, delta, duty, controlParameters, cLoopFeedback):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name, interval and zFlag status to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Boolean variable shared between taskUser and
                            taskEncoder for the zero command
        @param mFlag        Boolean variable shared between taskUser and
                            taskMotor for the motor1 duty cycle command
        @param MFlag        Boolean variable shared between taskUser and
                            taskMotor for the motor2 duty cycle command
        @param cFlag        Boolean variable shared between taskUser and
                            taskMotor for the reset fault command
        @param wFlag        Boolean variable shared between taskUser and
                            taskMotor for the closed loop control toggle command
        @param data         Tuple data shared between taskUser and
                            taskEncoder for data collection
        @param delta        Tuple shared between taskUser and
                            taskEncoder that stores delta data
        @param duty         Tuple shared between taskUser and
                            taskMotor that stores the input duty value
        @param controlParameters Tuple shared between taskUser and
                            taskMotor that stores the input reference velocity and proportional gain
        @param cLoopFeedback Tuple shared between taskUser and
                            taskMotor that stores the closed loop error feedback from each update
    '''
    state = S0_INIT
    next_time = ticks_add(ticks_us(), period)
    ser = USB_VCP()

    posArray = array.array('h', 3001*[0])
    gc.collect()
    timArray = array.array('l', 3001*[0])
    gc.collect()
    velArray = array.array('h', 3001*[0])

    buffer = ''
    
    numItem = 0
    numItemT = 0
    numItem2 = 0
    numItem3 = 0
    
    numPrint = 0
    numPrintT = 0
    numPrintS = 0
    
    velSUM = 0
    refVel = 0
    propGain = 0
    
    activeTest = False
    activeStep = False
    setPointActive = False
    setGainActive = False
    
    motor = ''
    
    while True:
       
        current_time = ticks_us()
       
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                printUI()
                state = S1_CMD

            elif state == S1_CMD:
                        
                if ser.any():
                    charIn = ser.read(1).decode()
                    if charIn in {'z', 'Z'}:
                        print('\nZeroing encoder... ')
                        zFlag.write(True)
                        state = S2_ZERO
                        
                    elif charIn in {'p', 'P'}:
                        print('\nPOSITION [rad]')
                        state = S3_PRINT
                        
                    elif charIn in {'d', 'D'}:
                        print('\nDELTA [rad]')
                        state = S4_DELTA
                        
                    elif charIn in {'g', 'G'}:
                        print('\nCOLLECT (Time [s], Position [rad], Velocity [rad/s])')
                        print('----- Data Collection Start -----')
                        state = S5_COLLECT
                        
                    elif charIn in {'v', 'V'}:
                        print('\nVELOCITY [rad/s]')
                        state = S6_VELOCITY
                        
                    elif charIn in {'m', 'M'}:
                        print('\nEnter Duty Cycle')
                        buffer = ''
                        if charIn == 'm':
                            motor = 'motor1'
                        else:
                            motor = 'motor2'
                        state = S7_GETDUTY
                        
                    elif charIn in {'c', 'C'}:
                        print('\nClearing Fault... ')
                        cFlag.write(True)
                        state = S9_FAULT
                        
                    elif charIn in {'t', 'T'}:
                        buffer = ''
                        print('\nEnter Duty Cycle For Testing')
                        activeTest = True
                        motor = 'motor1'
                        state = S7_GETDUTY
                        
                    elif charIn in {'y', 'Y'}:
                        buffer = ''
                        print('\nChoose Reference Velocity for Closed-Loop Control')
                        setPointActive = True
                        state = S7_GETDUTY
                        
                    elif charIn in {'k', 'K'}:
                        buffer = ''
                        print('\nChoose a Proportional Gain Coefficient')
                        setGainActive = True
                        state = S7_GETDUTY
                        
                    elif charIn in {'w', 'W'}:
                        print('\nToggling Closed Loop Control')
                        wFlag.write(True)
                        state = S12_CLOOP
                    
                    elif charIn in {'r', 'R'}:
                        print('\nChoose a Proportional Gain Coefficient')
                        activeStep = True
                        setGainActive = True
                        state = S7_GETDUTY
                    
                    else:
                        print('Press a valid key \n')
                        printUI()
                        continue
                   
            elif state == S2_ZERO:
                if not zFlag.read():
                    print('Encoder zeroed')
                    state = S1_CMD
                    
            elif state == S3_PRINT:
                nullTim, pos = data.read()
                print(f'{(pos*2*100*math.pi/4000):.2f}')
                print('Position printed')
                state = S1_CMD
                    
            elif state == S4_DELTA:
                delt = delta.read()
                print(f'{(delt*2*math.pi/4000):.4f}')
                print('Delta printed')
                state = S1_CMD
                    
            elif state == S5_COLLECT:
                if numItem < 3001:
                    timArray[numItem], posArray[numItem] = data.read()
                    velArray[numItem] = delta.read()
                    numItem += 1
                    if ser.any():
                        charIn = ser.read(1).decode()
                        if charIn in {'s', 'S'}:
                            state = S20_END
                    elif numItem == 3001:
                        state = S20_END
                    
            elif state == S6_VELOCITY:
                    delt = delta.read()
                    print(f'{(delt*.05*math.pi):.2f}')
                    print('Velocity Printed')
                    state = S1_CMD
                    
            elif state == S7_GETDUTY:

                if ser.any():
                    charIn = ser.read(1).decode()
                    if charIn in {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}:
                        buffer += charIn
                        print(buffer[-1], end='')

                    elif charIn == '-':
                        if len(buffer) == 0:
                            buffer += charIn
                            print(buffer[-1], end='')
                        else:
                            pass
                        
                    elif charIn == '.':
                        if buffer.count('.') == 0:
                            buffer += charIn
                            print(buffer[-1], end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}:
                        if len(buffer) == 0:
                            pass
                        else:
                            buffer = buffer[:-1]
                            print((len(buffer)+1)*'\b' + ' ', end='')
                            print(buffer, end='')
                            
                    elif charIn in {'\r', '\n'}:
                        motorDuty = float(buffer)
                        if not setGainActive and not setPointActive:
                            if motorDuty > 100:
                                motorDuty = 100
                            elif motorDuty < -100:
                                motorDuty = -100
                        else:
                            if motorDuty > 188.5:
                                motorDuty = 188.5
                            elif motorDuty < -188.5:
                                motorDuty = -188.5
                                
                        if not activeTest and not setPointActive and not setGainActive:
                            state = S1_CMD
                            
                        elif activeTest == True:
                            state = S8_TEST
                            print('\nRunning Test\n')
                            numItemT += 1
                            
                        elif setPointActive == True:
                            state = S10_REF
                            
                        elif setGainActive == True:
                            state = S11_SETGAIN
                        
                        buffer = '\n'
                        
                        if motor == 'motor1':
                            print('\nSetting duty cycle to: ' + str(motorDuty) + '%\n')
                            duty.write((motorDuty, 0))
                            mFlag.write(True)
                        elif motor == 'motor2':
                            print('\nSetting duty cycle to: ' + str(motorDuty) + '%\n')
                            duty.write((0, motorDuty))
                            MFlag.write(True)
                        
                            
                    elif charIn in {'s', 'S'}:
                        if activeTest == False:
                            print('Menu Return')
                            printUI()
                            state = S1_CMD
                        if activeTest == True:
                            state = S20_END
                            print('\n---------- START DATA ---------')
                            print('Duty Cycle [%], Velocity [rad/s]')
                            buffer = '\n'
                    
                    elif charIn in {'c', 'C'}:
                        state = S9_FAULT
                            
            elif state == S8_TEST:
                numItem2 += 1
                if numItem2 > 11 and numItem2 <= 31:
                    print('', end='---')
                    delt = delta.read()
                    velSUM += (delt * 0.05 * math.pi)

                    if numItem2 == 31:
                        velAVG = velSUM / 20
                        dutyDC = motorDuty
                        posArray[numItemT-1] = int(dutyDC)
                        timArray[numItemT-1] = int(velAVG*100)
                        print('\n')
                        buffer = ''
                        state = S7_GETDUTY
                        velSUM = 0
                        numItem2 = 0
                        print('Enter New Duty Cycle For Testing or Press S to Finish')
                
            elif state == S9_FAULT:
                if not cFlag.read():
                    print('Fault cleared')
                    state = S1_CMD
                    
            elif state == S10_REF:
                print('\nReference Velocity Set')
                refVel = motorDuty
                controlParameters.write((refVel, propGain))
                setPointActive = False
                state = S1_CMD
                
                if activeStep == True:
                    state = S14_STEP
                    cLoopFeedback.write((0,0))
                    temp, null = controlParameters.read()
                    controlParameters.write((0, propGain))
                    wFlag.write(True)
                
            elif state == S11_SETGAIN:
                print('\nProportional Gain Set')
                propGain = motorDuty
                controlParameters.write((refVel, propGain))
                setGainActive = False
                state = S1_CMD
                
                if activeStep == True:
                    print('\nChoose Reference Velocity for Closed-Loop Control')
                    state = S7_GETDUTY
                    setPointActive = True
                
            elif state == S12_CLOOP:
                if not wFlag.read():
                    state = S1_CMD
                    
            elif state == S14_STEP:
                timArray[numItem3], null = data.read()
                numItem3 += 1
                if numItem3 <= 101:
                    posArray[numItem3-1] = 0
                    velArray[numItem3-1] = 0
                    if numItem3 == 101:
                        controlParameters.write((temp, propGain))
                        
                elif numItem3 > 101 and numItem3 <= 301:
                    a, b = cLoopFeedback.read()
                    a = int(a*100)
                    b = int(b*100)
                    posArray[numItem3], velArray[numItem3] = a, b
                    
                    if ser.any():
                        charIn = ser.read(1).decode()
                        if charIn in {'s', 'S'}:
                            state = S20_END
                    elif numItem3 == 301:
                        state = S20_END
                        print('\n(Time [s], Duty Cycle [%], Velocity [rad/s])')
                        print('----- Data Collection Start -----')
                        
                           
            elif state == S20_END:
                if numItem == 0:
                    state = S1_CMD

                elif numPrint < numItem:
                    print(f'{((timArray[numPrint]/1000000-timArray[0]/1000000)):.2f}', \
                          f'{(posArray[numPrint]*2*100*math.pi/4000):.2f}', \
                          f'{(velArray[numPrint]*.05*math.pi):.2f}')
                    numPrint += 1

                elif numPrint == numItem:
                    print('----- Data collection ended -----')
                    state = S1_CMD
                    numItem = 0
                    numPrint = 0
                    
                if activeTest == True:
                    if numPrintT < numItemT:
                        print('        ', end='')
                        print(f'{(posArray[numPrintT]):.2f}, {(timArray[numPrintT]/100):.2f}')
                        numPrintT += 1
                        state = S20_END
                    
                    elif numPrintT == numItemT:
                        print('---------- END DATA ----------')
                        state = S1_CMD
                        numItemT = 0
                        numPrintT = 0
                        numItem2 = 0
                        activeTest = False
                        motor = ''
                
                if activeStep == True:
                    controlParameters.write((0, propGain))
                    if numPrintS < numItem3:
                        print(f'{((timArray[numPrintS]/1000000-timArray[0]/1000000)):.2f}', \
                                 f'{(posArray[numPrintS]/100):.2f}', \
                                 f'{(velArray[numPrintS]/100):.2f}')
                        numPrintS += 1
                        state = S20_END
                    
                    elif numPrintS == numItem3:
                        print('---------- END DATA ----------')
                        state = S1_CMD
                        numPrintS = 0
                        numItem3 = 0
                        activeStep = False
                        wFlag.write(True)

            next_time = ticks_add(next_time, period)
                
            yield state
       
        else:
            yield None
       
       
def printUI():
    '''!@brief Prints the command screen for the user interface
    '''
    print('+----------------------------------------+\n'
          '| ME 305 Position Encoder User Interface |\n'
          '+----------------------------------------+\n'
          '|              |Commands|                |\n'
          '|z or Z   - Zero the Encoder             |\n'
          '|p or P   - Print Position               |\n'
          '|d or D   - Print Position Change        |\n'
          '|v or V   - Print Velocity               |\n'
          '|m        - Change Motor 1 Speed         |\n'
          '|M        - Change Motor 2 Speed         |\n'
          '|c or C   - Clear Motor Fault            |\n'
          '|g or G   - Collect and Print Encoder    |\n'
          '|           Position for 30 Seconds      |\n'
          '|t or T   - Test and Print Duty Cycle    |\n'
          '|           and Corresponding Velocities |\n'
          '|s or S   - End Data Collection Early    |\n'
          '|y or Y   - Choose Set Point for Control |\n'
          '|k or K   - Enter Gain                   |\n'
          '|w or W   - Toggle Closed Loop Control   |\n'
          '|r or R   - Collect and Print Closed Loop|\n'
          '|           Controlled Motor Response to |\n'
          '|           a Step Input                 |\n'
          '|Ctrl + c - Terminate the Program        |\n'
          '+----------------------------------------+')
