'''!@file       mainOLD.py
    @brief      Runs all the encoder files together
    @details    Acts as a central point or interface for all the encoder
                files to share their values between each other and run
                through a list of tasks.

                \n The Task Diagram for the value sharing between the three task files can be seen here.
                @image html encoderTaskDiagram_FSM.PNG width=500px
                @image html motorTaskDiagram_FSM.PNG width=500px
                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x02/mainOLD.py
    @author     Lucas Murray
    @author     Max Cassady
    @date       February 24, 2022
'''

from taskEncoder import taskEncoderFcn
from taskUser import taskUserFcn
from taskMotor import taskMotorFcn
import shares, driver

## @brief Initiates the shared variable for the zero flag
#
zFlag = shares.Share()

## @brief Initiates the shared variable for the motor1 flag
#
mFlag = shares.Share()

## @brief Initiates the shared variable for the motor2 flag
#
MFlag = shares.Share()

## @brief Initiates the shared variable for the motor's fault-state
#
cFlag = shares.Share()

## @brief Initiates the shared variable for closed loop control toggling
#
wFlag = shares.Share()

## @brief Initiates the shared variable for the time and position data
#
data = shares.Share((0, 0))

## @brief Initiates the shared variable for the chane in position data
#
delta = shares.Share(0)

## @brief Initiates the shared variable for the motor duty cycle data
#
duty = shares.Share((0, 0))

## @brief Initiates the shared variable for the reference velocity and gain data for the closed loop controller
#
controlParameters = shares.Share((100, 0.5))

## @brief Initiates the shared variable for the closed loop controller feedback data
#
cLoopFeedback = shares.Share((0, 0))

if __name__ == '__main__':

    ## @brief Stores a list of the MCU functions at the desired frequency to be run simultaneously
    #
    taskList = [taskEncoderFcn('taskEncoder', 10_000, zFlag, data, delta),
                taskUserFcn('taskUser', 10_000, zFlag, mFlag, MFlag, cFlag, wFlag, data, delta, duty, controlParameters, cLoopFeedback),
                taskMotorFcn('taskMotor', 10_000,  mFlag, MFlag, cFlag, wFlag, duty, delta, controlParameters, cLoopFeedback)]

    while True:        # Runs the tasks indefinitely
        try:
            for task in taskList:
                next(task)

        except KeyboardInterrupt:
            driver.DRV8847(3).disable()
            break

    print('Program Terminating')