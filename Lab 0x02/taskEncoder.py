"""!@file       taskEncoder.py
    @brief      Runs tasks sent by the taskUser file and keeps the encoder updated.
    @details    Acts as a background function file controlling the encoder for the primary user interface
                so that the user interface does not have to perform tasks directly.
                It will continuously update the encoder every 10 milliseconds.

                /n The State Transition Diagram for taskEncoder can be seen here.
                @image html taskEncoder_FSM.PNG width=1000px
                @image html task_I_O_FSM.PNG width=500px
                @image html taskEncoder_O.PNG width=500px

                /n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x02/taskEncoder.py
    @author     Lucas Murray
    @author     Max Cassady
    @date       February 16, 2022
"""

import pyb
from time import ticks_us, ticks_add, ticks_diff
import micropython
from encoder import Encoder
import time

## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Update variable as the constant 1
#
S1_UPDATE = micropython.const(1)

## @brief Defines the State 2 Zero variable as the constant 2
#
S2_ZERO = micropython.const(2)


def taskEncoderFcn(taskName, period, zFlag, data, delta):
    '''!@brief              Performs the tasks as determined by the taskUser file
        @details            Interfaces with the taskUser file through the use of
                            the shared flag parameters, keeping the encoder
                            updated and performing the input commands
        @param taskName     The name of the task a string
        @param period       The period of the task in microseconds as an integer
        @param zFlag        Boolean variable shared between taskUser and
                            taskEncoder for the zero command
        @param data         Tuple data shared between taskUser and
                            taskEncoder for data collection
        @param delta        Tuple shared between taskUser and
                            taskEncoder that stores delta data
    '''

    ## @brief Initiates the first pin needed for the encoder at pin B6
    #
    B6 = pyb.Pin(pyb.Pin.cpu.B6)

    ## @brief Initiates the second pin needed for the encoder at pin B7
    #
    B7 = pyb.Pin(pyb.Pin.cpu.B7)

    ## @brief Instantiates an encoder object for use in taskEncoder
    #
    myEncoder = Encoder(B6, B7, 4)

    ## @brief Begins the function at State 0 before starting the loop
    #
    state = S0_INIT

    ## @brief Stores the time at which the function began
    #
    start_time = ticks_us()

    ## @brief Stores the value for the next time that the while loop should
    #         be run
    #
    next_time = ticks_add(ticks_us(), period)
    
    ## @brief Initiates the position used for the encoder update command
    #
    updatePosition = 0
    

    while True:
        current_time = ticks_us()                       # Keeps updating current time
        
        if ticks_diff(current_time, next_time) >= 0:    # Waits for current_time to pass next_time
            
            if state == S0_INIT:
                state = S1_UPDATE
                
                
            elif state == S1_UPDATE:
                update_time = time.ticks_us()
                elapsedTime = time.ticks_diff(update_time, start_time)
                data.write((elapsedTime, int(myEncoder.get_position()/100)))
                delta.write(myEncoder.get_delta())
                
                [updatePosition] = myEncoder.update(updatePosition)         # Updates the encoder position
                
                if zFlag.read():
                    state = S2_ZERO
                
            elif state == S2_ZERO:
                myEncoder.zero()
                zFlag.write(False)
                state = S1_UPDATE
            
            else:
                raise ValueError(f'Invalid State in {taskName}')
                
            next_time = ticks_add(next_time, period)
            
            yield state
            
        else:
            yield None
