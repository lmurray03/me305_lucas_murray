"""!@file       taskMotor_F.py
    @brief      Runs tasks sent by the taskUser file and performs ball balancing.
    @details    Acts as a background function file controlling the platform motors for the primary user interface
                so that the user interface does not have to motor actuation and closed loop control by itself. When
                closed loop control is activated by the taskUser, it will actively perform closed loop control to
                balance the ball and board. When there is no contact with the touch panel on the platform, it will
                only run the inner loop control and solely balance the platform, but when there is contact, the
                outer loop and inner loop will run sequentially, attempting to balance the ball in response to the
                touch panel position data.

                \n The State Transition Diagram for taskMotor_F can be seen here.
                @image html MOTORFSM.PNG width=1000px
                @image html MOTORFSMIO.PNG width=500px

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Term%20Project/taskMotor_F.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       March 17, 2022
"""

from pyb import Pin, Timer
from time import ticks_us, ticks_add, ticks_diff
import micropython, closedLoopControl_Term  #, random
from motor import Motor


## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Wait variable as the constant 1
#
S1_WAIT = micropython.const(1)

## @brief Defines the State 2 Gain variable as the constant 2
#
S2_GAIN = micropython.const(2)

## @brief Defines the State 3 Balance variable as the constant 3
#
S3_BALANCE = micropython.const(3)

## @brief Defines the state 4 Haptic Feedback variable as the constant 4
#
S4_HAPTIC = micropython.const(4)


def taskMotorFcn(taskName, period, wFlag, kFlag, hFlag, cFlag, duty, Eang, Vang, Poxy, Voxy, Cart, Cast, controlParameters):
    '''!@brief              Runs the relevant motor tasks as determined by taskUser and runs closed loop control
        @details            Interfaces with the taskUser file through the use of
                            the shared flag parameters and shared tuples, being able to change the gain coefficients of
                            either closed loop controller as well as toggling and updating each closed loop controller
                            actively depending on whether there is contact with the board.
        @param taskName     The name of the task a string
        @param period       The period of the task in microseconds as an integer
        @param wFlag        Boolean variable shared between taskUser and
                            taskMotor for the closed loop control toggle command
        @param kFlag        Tuple variable shared between taskUser and
                            taskMotor for the change gains command
        @param hFlag        Boolean variable shared between taskPanel and
                            taskMotor for the haptic feedback command
        @param cFlag        Boolean variable shared between taskUser and
                            taskIMU for the IMU calibration command
        @param Eang         Tuple shared between taskIMU, taskUser, taskData, and taskMotor
                            that stores euler angle values
        @param Vang         Tuple shared between taskIMU, taskUser, taskData, and taskMotor
                            that stores angular velocity values
        @param Poxy         Tuple shared between taskMotor and taskPanel
                            that stores the touch panel xy position values
        @param Voxy         Tuple shared between taskMotor and taskPanel
                            that stores the touch panel xy velocity values
        @param Cart         Tuple shared between taskData and
                            taskPanel that contains the touch panel position
                            and velocity data values
        @param Cast         Tuple shared between taskUser and
                            taskEncoder that would have allowed for the automated calibration of the IMU
        @param controlParameters Tuple shared between taskUser and
                            taskMotor that stores the input reference velocity and proportional gain
    '''

    state = S0_INIT
    
    PWM_tim = Timer(3, freq = 20_000)
    
    # ----------------- Motor 1 pins --------------------
    pinB4 = Pin(Pin.cpu.B4, Pin.OUT_PP)
    pinB5 = Pin(Pin.cpu.B5, Pin.OUT_PP)
    
    # ----------------- Motor 2 pins --------------------
    pinB0 = Pin(Pin.cpu.B0, Pin.OUT_PP)
    pinB1 = Pin(Pin.cpu.B1, Pin.OUT_PP)
    
    # ------------------ Motor objects ------------------
    motor1 = Motor(PWM_tim, pinB4, pinB5, 1, 2)
    motor2 = Motor(PWM_tim, pinB0, pinB1, 3, 4)
    
    # --------------- Time Initialization ---------------
    next_time = ticks_add(ticks_us(), period)
    
    # ---------- Closed Loop Control Variables ----------
    cloopIn = closedLoopControl_Term.ClosedLoop(0, 0, 2.5, 0.25, 0.1, Vang.read(), Eang.read())
    cloopOut = closedLoopControl_Term.ClosedLoop(0, 0, 1, 0.1, 0.1, Vang.read(), Eang.read())
    
    # -------------- Toggle Initialization --------------
    activeToggle = False
    toggle = 0
    
    while True:

        
        current_time = ticks_us()                       # Keeps updating current time
        
        if ticks_diff(current_time, next_time) >= 0:    # Waits for current_time to pass next_time

            if state == S0_INIT:
                state = S1_WAIT
            

            # -------------------- State Switcher --------------------
            elif state == S1_WAIT:
                if wFlag.read():
                    
                    toggle += 1
                    if toggle == 1:
                        activeToggle = True
                        print('\nClosed Loop Control Activated')
                    elif toggle == 2:
                        activeToggle = False
                        print('\nClosed Loop Control Deactivated')
                        toggle = 0
                        
                elif kFlag.read() == 1:
                    state = S2_GAIN
                    
                elif kFlag.read() == 2:
                    state = S2_GAIN
                    
                elif hFlag.read():
                    state = S4_HAPTIC
                    
                if activeToggle:
                    null, null, zScan, null, null = Cart.read()
                    state = S3_BALANCE
                    
                wFlag.write(False)
                

            # ---------- Set Closed Loop Gain Coefficient ----------
            elif state == S2_GAIN:
                if kFlag.read() == 1:
                    a, b, Kp, Kd, Ki = controlParameters.read()
                    cloopIn.setGains(Kp, Kd, Ki)
                    kFlag.write(0)
                    state = S1_WAIT
                
                elif kFlag.read() == 2:
                    a, b, Kp, Kd, Ki = controlParameters.read()
                    cloopOut.setGains(Kp, Kd, Ki)
                    kFlag.write(0)
                    state = S1_WAIT
                

            elif state == S3_BALANCE:
                null, null, zScan, null, null = Cart.read()

                # ------------ Ball Balancing ------------
                if zScan == True:
                    
                    # --------------------Outer Loop Control--------------------
                    cloopOut.update(Voxy, Poxy)
                    thetaX, thetaY = cloopOut.motorDutyX, cloopOut.motorDutyY
                    alim = 10
                    if thetaX > alim:
                        thetaX = alim
                    elif thetaX < -alim:
                        thetaX = -alim
                    
                    if thetaY > alim:
                        thetaY = alim
                    elif thetaY < -alim:
                        thetaY = -alim
                        
                    cloopIn.setRefPos(-1*thetaX, thetaY)
                    

                    # -------------------- Inner Loop Control --------------------
                    cloopIn.update(Vang, Eang)
                    
                    motor1.setDuty(cloopIn.motorDutyX)
                    motor2.setDuty(cloopIn.motorDutyY)
                    

                # ---------- Platform Balancing ----------
                elif zScan == False:

                    # -------------------- Inner Loop Control --------------------
                    cloopIn.setRefPos(0,0)
                    cloopIn.update(Vang, Eang)
                    
                    null, null, zScan, null, null = Cart.read()
                    motor1.setDuty(cloopIn.motorDutyX)
                    motor2.setDuty(cloopIn.motorDutyY)
                    
                state = S1_WAIT
                    

            # ---------- Haptic Feedback for Touch Panel Calibration ----------
            elif state == S4_HAPTIC:
                motor1.Haptic()
                hFlag.write(False)
                state = S1_WAIT


            else:
                raise ValueError(f'Invalid State in {taskName}')
        
            next_time = ticks_add(next_time, period)
            
            yield state
            
        else:
            yield None