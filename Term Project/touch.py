"""!@file       touch.py
    @brief      A driver that runs and controls a resistive touch panel.
    @details    The driver class instantiates a driver that can instantiate and control an Analog to Digital Converter
                (ADC) device and calibrate it. The driver uses the pyb library functionality for an ADC to read the
                voltage running between two pins, each set to one of the four pins on the touch panel (-x,+x,-y,+y),
                and converts that voltage reading to an x and y position on the board. The velocity can then be found
                using the change in position. The Touch class can also check for contact with the board, being able to
                return a boolean value for it. Both the position and velocity use alpha-beta filtering, offsets, and
                scaling values to correct the theoretical position of the touch panel and line it up with the actual
                one. The driver can also read the calibration status and either set the scaling values and offsets from
                an already written file or write a new calibration file. The Touch class will update the xy positions
                and xy velocities read and converted to the values in mm and mm/s.

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Term%20Project/touch.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       March 17, 2022
"""

from pyb import Pin, ADC
from time import ticks_us, ticks_diff
import os

class Touch:
    
    def __init__(self,xP,xM,yP,yM,a,b,x0,y0):
        '''!@brief Initializes and returns a resistive touch panel object.
            @param xP The initial x+ pin configuration
            @param xM The initial x- pin configuration
            @param yP The initial y+ pin configuration
            @param yM The initial y- pin configuration
            @param a  The alpha value for the alpha-beta filtering
            @param b  The beta value for the alpha-beta filtering
            @param x0 The initial x-direction offset
            @param y0 The initial y-direction offset
        '''
        # ---------- Pin Creation ----------
        self.xP = xP
        self.xM = xM
        self.yP = yP
        self.yM = yM
        
        # ---- Coefficient Instantiation ---
        self.Kxx = 1
        self.Kxy = 0
        self.Kyx = 0
        self.Kyy = 1
        
        # ----- a/b Filter Coefficients ----
        self.a = a
        self.b = b

        # --- Panel Offset Instantiation ---
        self.x0 = x0
        self.y0 = y0
        

        self.count = 1          # Tracks Calibration Step
        self.nxt = False        # Allows Calibration Stepping
        self.lastScan = 'z'     # First Scan is zScan()
    
    
    # ----------------- Panel Contact Check ------------------
    def zScan(self):
        '''!@brief Scans for a voltage in the z-direction from an ADC
            @details Sets the touch panel pins and ADC to the necessary configurations and
                     reads the ADC for contact with the board by measuring z-axis voltage
            @return Boolean True if there is contact with the board, otherwise false
        '''
        if self.lastScan == 'z':
            pass
        elif self.lastScan == 'y':
            self.yM.init(self.yM.IN)
            self.xM.init(self.xM.OUT_PP,value=0)
        elif self.lastScan == 'x':
            self.xP.init(self.xP.IN)
            self.yP.init(self.yP.OUT_PP,value=1)
        else:
            self.xP.init(self.xP.IN)
            self.xM.init(self.xM.OUT_PP,value=0)
            
            self.yP.init(self.yP.OUT_PP,value=1)
            self.yM.init(self.yM.IN)

        self.lastScan = 'z'
        
        if ADC(self.yP).read() < 500:
            return True
        else:
            return False
    

    # ----------------- Contact X Voltage -----------------
    def xScan(self):
        '''!@brief Scans for a voltage in the x-direction from an ADC
            @details Sets the touch panel pins and ADC to the necessary configurations and
                     reads the ADC for a voltage in the x-direction
            @return list Scaled values of the ADC reading
        '''
        if self.lastScan == 'x':
            pass
        elif self.lastScan == 'z':
            self.xP.init(self.xP.OUT_PP,value=1)
            self.yP.init(self.yP.IN)
        else:
            self.xP.init(self.xP.OUT_PP,value=1)
            self.xM.init(self.xM.OUT_PP,value=0)
            
            self.yP.init(self.yP.IN)
            self.yM.init(self.yM.IN)
        
        self.lastScan = 'x'
        vX = ADC(self.yP)
        
        return(vX.read()*self.Kxx, vX.read()*self.Kyx)
    
    
    # ----------------- Contact Y Voltage -----------------
    def yScan(self):
        '''!@brief Scans for a voltage in the y-direction from an ADC
            @details Sets the touch panel pins and ADC to the necessary configurations and
                     reads the ADC for a voltage in the y-direction
            @return list Scaled values of the ADC reading
        '''
        if self.lastScan == 'y':
            pass
        elif self.lastScan == 'z':
            self.yM.init(self.yM.OUT_PP,value=0)
            self.xM.init(self.xM.IN)
        else:
            self.yP.init(self.yP.OUT_PP,value=1)
            self.yM.init(self.yM.OUT_PP,value=0)
            
            self.xP.init(self.xP.IN)
            self.xM.init(self.xM.IN)
        
        self.lastScan = 'y'
        vY = ADC(self.xP)
        
        return(vY.read()*self.Kxy, vY.read()*self.Kyy)
        

    # ----------- Translate Voltage to Position -----------
    def cart(self):
        '''!@brief Runs all three scans and converts data into usable values
            @details Takes the average of ten converted position values for each scan and performs alpha-beta filtering
            to minimize effects of bad measurement values and gather position data. It then accounts for the offsets to
            get the true positions.
            @return list Converted position, zScan, and velocity values
        '''
        while True:
            
            # --------- Generate Initial Position ---------
            itr = 1
            xTch = 0
            yTch = 0
            xVel = 0
            yVel = 0
            
            xxTch, yxTch = self.xScan()
            xyTch, yyTch = self.yScan()
            
            xPos = (xxTch + xyTch)/4096*176 - self.x0
            yPos = (yyTch + yxTch)/4096*100 - self.y0
            
            strTime = ticks_us()
            
            # ----------------- a/b Filter -----------------
            while itr <= 10:
                
                xxTch, yxTch = self.xScan()
                xyTch, yyTch = self.yScan()

                xTch += (xxTch + xyTch)/4096*176 - self.x0
                yTch += (yyTch + yxTch)/4096*100 - self.y0
                
                itr += 1
                
            if itr == 11:
                
                TS = ticks_diff(ticks_us(), strTime)/1000000
                
                xMes = xTch / 10
                yMes = yTch / 10
                
                xPosk = xPos
                yPosk = yPos
                xPos = xPos + self.a*(xMes - xPos) + TS*xVel
                yPos = yPos + self.a*(yMes - yPos) + TS*yVel
                
                xVel = xVel + self.b/TS * (xPosk - xMes)
                yVel = yVel + self.b/TS * (yPosk - yMes)
                break
                
        # ---------- Return Position and Velocity ----------
        return(xPos-87.5, yPos-47, self.zScan(), xVel, yVel)
        
    
    # ------------------ Panel Calibration -----------------
    def calibrate(self, hFlag):
        '''!@brief Calibrates the resistive touch panel
            @details Takes the average of one hundred measurements at three different board positions to accurately
                     calculate the scaling factors and offsets needed to line up the theoretical board with the actual.
            @param hFlag    Boolean variable shared between taskPanel and
                            taskMotor for the haptic feedback command
            @return Boolean True when the calibration is finished
        '''
        if self.count == 1:       # Touch Center of Panel
            itr = 1
            xPos, yPos = (0, 0)
            
            while self.zScan() == True:
                if itr < 101:
                    xCal, yCal, null, null, null = self.cart()
                    xPos += xCal
                    yPos += yCal
                    itr += 1
                    
                elif itr == 101:
                    xPos /= 100
                    yPos /= 100
                    break
                
            if itr == 101:  # Averages 100 position measurements
                print('REMOVE CALIBRATION INSTRUMENT')
                
                hFlag.write(True)
                self.nxt = True
                
                # Setting Offset Coefficients
                self.x0 = xPos
                self.y0 = yPos
                
            if self.zScan() == False and self.nxt == True:
                self.count += 1   
                print('\nTouch Bottom Left')
                self.nxt = False
                
                
                
        elif self.count == 2:       # Touch Bottom Left of Panel
            itr = 1
            xPos, yPos = (0, 0)
            
            while self.zScan() == True:
                if itr < 101:
                    xCal, yCal, null, null, null = self.cart()
                    xPos += xCal
                    yPos += yCal
                    itr += 1
                    
                elif itr == 101:
                    xPos /= 100
                    yPos /= 100
                    break
                
            if itr == 101:
                print('REMOVE CALIBRATION INSTRUMENT')
                #print(xPos, yPos)
                hFlag.write(True)
                self.nxt = True
                
                # ---------- Setting Uncoupled Coefficients ----------
                self.Kxx = -80/xPos
                self.Kyy = -40/yPos
                self.x0 += 10
                self.y0 += 13.5
                
                # ----------- Setting Coupled Coefficients -----------
                #self.Kxy = (xPos - self.x0 + self.Kxx*80) / 40
                #self.Kyx = (yPos - self.y0 + self.Kyy*40) / 80
                #print(self.Kxx, self.Kyy, self.Kxy, self.Kyx, self.x0, self.y0)
                
            if self.zScan() == False and self.nxt == True:
                self.count += 1   
                print('\nTouch Top Right')
                self.nxt = False
                
                
        # -------------------- UNDER CONSTRUCTION --------------------
        elif self.count == 3:       # Touch Top Right of Panel
            itr = 1
            xPos, yPos = (0, 0)
            
            while self.zScan() == True:
                if itr < 101:
                    xCal, yCal, null, null, null = self.cart()
                    xPos += xCal
                    yPos += yCal
                    itr += 1
                    
                elif itr == 101:
                    xPos /= 100
                    yPos /= 100
                    
                    # ---------- Setting Uncoupled Coefficients ----------
                    #self.Kxx = (self.Kxx + (self.x0 + xPos) / 80) / 2
                    #self.Kyy = (self.Kyy + (self.y0 + yPos) / 40) / 2
                    #print(self.Kxx, self.Kyy)
                    break

                
            if itr == 101:
                print('REMOVE CALIBRATION INSTRUMENT')
                #print(xPos, yPos)
                hFlag.write(True)
                self.nxt = True
                
            if self.zScan() == False and self.nxt == True:
                self.count = 1
                self.nxt = False
                self.setCalCoefficients()
                return True
        self.calStatus = True
        

    # -------------- Write Panel Coefficients to File --------------
    def setCalCoefficients(self):
        '''!@brief Writes the newly obtained calibration coefficients of the touch panel to a new file
            @details Only accessed when getCalCoefficients() doesn't find a calibration file, it will run the
            calibrate() function and write the necessary values to a new file with the required name.
        '''
        self.coefficients = [self.Kxx, self.Kxy, self.Kyx, self.Kyy, self.x0, self.y0]
        
        with open("Touch_cal_coeffs.txt", 'w') as f:
            (A, B, C, D, E, F) = self.coefficients
            lst = (A, B, C, D, E, F)
            string = ''
            for item in lst:
                item = str(item)
                string += (item + ',')
            string = string[:-1]
            
            f.write(string)  
            print('Creating new calibration file')
            

    # -------------- Read Panel Coefficients From File --------------
    def getCalCoefficients(self):
        '''!@brief Sets the calibration coefficients of the touch panel from a file or from manual calibration
            @details If the calibration file already exists, it will read it and set the calibration coefficients
                     of the touch panel from the read data, and if the file doesn't exist, it runs the calibration
                     process and sends the new touch panel coefficients over to setCalCoefficients().
        '''
        file = "Touch_cal_coeffs.txt"
        if file in os.listdir():
            
            with open(file, 'r') as f:
                calDataString = f.readline()
                calValues = [float(calValue) for calValue in calDataString.strip().split(',')]
                #calValues = ''.join(calValues)
                
                self.Kxx, self.Kxy, self.Kyx, self.Kyy, self.x0, self.y0  = calValues
                
                print('Calibration file read')
                return True
        
        else:
            
            return False
                
 
if __name__ == '__main__':

    a = 0.85
    b = 0.005
    x0 = 0
    y0 = 0

    # X Pins
    xP  = Pin(Pin.cpu.A7, Pin.IN)
    xM  = Pin(Pin.cpu.A1, Pin.OUT_PP,value=0)
    
    # Y Pins
    yP = Pin(Pin.cpu.A6, Pin.OUT_PP,value=1)
    yM  = Pin(Pin.cpu.A0, Pin.IN)
    
    tch = Touch(xP, xM, yP, yM, a, b, x0, y0)
    
    xTch = 0
    yTch = 0

    '''
    while True:

        if tch.zScan() == True:
            print(tch.cart())
    '''
    #---------------------Tests XYZ Scan Speed--------------------------
    strTime = ticks_us()
    numItem = 0
    while numItem <= 1000:
        tch.zScan()
        tch.xScan()
        tch.yScan()
        numItem += 1
    
    print((ticks_diff(ticks_us(), strTime))/1000)

