"""!@file       taskData.py
    @brief      Runs tasks sent by the taskUser file and collects the desired data.
    @details    Acts as a background function file controlling the IMU for the primary user interface
                so that the user interface does not have to perform data collection directly. It will write the
                chosen data to a file for 20 seconds that can be used to plot or just saved for later.
                \n\n Sample Data Plots
                The platform can gather various different forms of data through this task, including the ball position,
                the ball velocity, the platform angular position, and the platform angular velocity. Sample plots from
                the file created by each of these data collections can be seen below. The data collected has no
                real meaning, it was just us moving the ball and platform around, but it acts as proof that our data
                collection works.
                @image html DATAPLT1.png width=500px
                @image html DATAPLT2.png width=500px
                @image html DATAPLT3.png width=500px
                @image html DATAPLT4.png width=500px

                \n\n The State Transition Diagram for taskData can be seen here.
                @image html DATAFSM.PNG width=1000px
                @image html DATAFSMIO.PNG width=500px

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Term%20Project/taskData.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       March 17, 2022
"""

import micropython, array
from time import ticks_us, ticks_add, ticks_diff

## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Data variable as the constant 1
#
S1_DATA = micropython.const(1)

## @brief Defines the State 2 Position variable as the constant 2
#
S2_POSITION = micropython.const(2)

## @brief Defines the State 3 Velocity variable as the constant 3
#
S3_VELOCITY = micropython.const(3)

## @brief Defines the State 4 Angle variable as the constant 4
#
S4_ANGLE = micropython.const(4)

## @brief Defines the State 5 Omega variable as the constant 5
#
S5_OMEGA = micropython.const(5)

## @brief Defines the State 6 Write variable as the constant 6
#
S6_WRITE = micropython.const(6)


def taskDataFcn(taskName, period, dFlag, sFlag, Eang, Vang, Cart):
    '''!@brief              Collects the data as determined by the taskUser file
        @details            Interfaces with the taskUser file through the use of
                            the shared flag and tuple parameters, reading the chosen data
                            for 20 seconds and writing it to a file unless told to stop
                            by the userTask.
        @param taskName     The name of the task a string
        @param period       The period of the task in microseconds as an integer
        @param dFlag        Tuple variable shared between taskUser and
                            taskData for the collect data command
        @param sFlag        Boolean variable shared between taskUser and
                            taskData for the stop data collection early command
        @param Eang         Tuple shared between taskIMU, taskUser, taskData, and taskMotor
                            that stores euler angle values
        @param Vang         Tuple shared between taskIMU, taskUser, taskData, and taskMotor
                            that stores angular velocity values
        @param Cart         Tuple shared between taskData and
                            taskPanel that contains the touch panel position
                            and velocity data values
        '''
    state = S0_INIT
    
    # -------------------- Timing Instantiation --------------------
    next_time   = ticks_add(ticks_us(), period)
    strt_time   = ticks_us()
    actual_time = 0
    
    # ------------------ Data Array Instantiation ------------------
    timArray    = array.array('l', 1001*[0])
    dataX       = array.array('h', 1001*[0])
    dataY       = array.array('h', 1001*[0])
    

    while True:
        
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
        
            if state == S0_INIT:
                state = S1_DATA
                

            # ---------- Data Type Collection Switcher ----------
            elif state == S1_DATA:
                
                if dFlag.read() == 2:
                    state = S2_POSITION
                    
                elif dFlag.read() == 3:
                    state = S3_VELOCITY
                    
                elif dFlag.read() == 4:
                    state = S4_ANGLE
                    
                elif dFlag.read() == 5:
                    state = S5_OMEGA
                
                itr = 0
                
            
            # ---------- Collect Position Data ----------
            elif state == S2_POSITION:
                update_time = ticks_us()
                actual_time = ticks_diff(update_time, strt_time)/1000
                if itr < 1001:
                    x, y, null, null, null = Cart.read()
                    timArray[itr] = int(actual_time)
                    dataX[itr]    = int(x*1000)
                    dataY[itr]    = int(y*1000)
                    itr += 1
                if itr == 1001 or sFlag.read() == True:
                    sFlag.write(False)
                    state = S6_WRITE
            

            # ---------- Collect Velocity Data ----------
            elif state == S3_VELOCITY:
                update_time = ticks_us()
                actual_time = ticks_diff(update_time, strt_time)/1000
                if itr < 1001:
                    null, null, null, x, y = Cart.read()
                    timArray[itr] = int(actual_time)
                    dataX[itr]    = int(x*1000)
                    dataY[itr]    = int(y*1000)
                    itr += 1
                if itr == 1001 or sFlag.read() == True:
                    sFlag.write(False)
                    state = S6_WRITE
            

            # ------------ Collect Euler Data ------------
            elif state == S4_ANGLE:
                update_time = ticks_us()
                actual_time = ticks_diff(update_time, strt_time)/1000
                if itr < 1001:
                    x, y, null = Eang.read()
                    timArray[itr] = int(actual_time)
                    dataX[itr]    = int(x*1000)
                    dataY[itr]    = int(y*1000)
                    itr += 1
                if itr == 1001 or sFlag.read() == True:
                    sFlag.write(False)
                    state = S6_WRITE
            

            # ---------- Collect Euler Rate Data ----------
            elif state == S5_OMEGA:
                update_time = ticks_us()
                actual_time = ticks_diff(update_time, strt_time)/1000
                if itr < 1001:
                    x, y, null = Vang.read()
                    timArray[itr] = int(actual_time)
                    dataX[itr]    = int(x*1000)
                    dataY[itr]    = int(y*1000)
                    itr += 1
                if itr == 1001 or sFlag.read() == True:
                    sFlag.write(False)
                    state = S6_WRITE
            

            # ----------- Write Data to File -------------
            elif state == S6_WRITE:
                if dFlag.read() == 2:
                    with open('POS_Data.txt', 'w') as f:
                        print('Creating File')
                        for itr in range(itr):
                            f.write(f"{((timArray[itr]-timArray[0])/1000)}, {(dataX[itr]/1000)}, {(dataY[itr]/1000)}\n")
                        print('File Created')
                
                if dFlag.read() == 3:
                    with open('VEL_Data.txt', 'w') as f:
                        print('Creating File')
                        for itr in range(itr):
                            f.write(f"{((timArray[itr]-timArray[0])/1000)}, {(dataX[itr]/1000)}, {(dataY[itr]/1000)}\n")
                        print('File Created')
                            
                if dFlag.read() == 4:
                    with open('ENG_Data.txt', 'w') as f:
                        print('Creating File')
                        for itr in range(itr):
                            f.write(f"{((timArray[itr]-timArray[0])/1000)}, {(dataX[itr]/1000)}, {(dataY[itr]/1000)}\n")
                        print('File Created')
                            
                if dFlag.read() == 5:
                    with open('VNG_Data.txt', 'w') as f:
                        print('Creating File')
                        for itr in range(itr):
                            f.write(f"{((timArray[itr]-timArray[0])/1000)}, {(dataX[itr]/1000)}, {(dataY[itr]/1000)}\n")
                        print('File Created')
                
                dFlag.write(0)
                state = S1_DATA

        
            else:
                raise ValueError(f'Invalid State in {taskName}')
            
            next_time = ticks_add(next_time, period)
            yield state
            
        else:
            yield None