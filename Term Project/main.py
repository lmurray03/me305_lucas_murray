'''!@file       main.py
    @brief      Runs all the term project files together
    @details    Acts as a central point or interface for all the term project
                files to share their values between each other and run
                through a list of tasks.

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Term%20Project/main.py
    @author     Lucas Murray
    @author     Max Cassady
    @date       March 17, 2022
'''

from taskUser_F import taskUserFcn
from taskMotor_F import taskMotorFcn
from taskIMU import taskIMUFcn
from taskPanel import taskPanelFcn
from taskData import taskDataFcn
import shares

## @brief Initiates the shared variable for closed loop control toggling
#
wFlag = shares.Share()

## @brief Initiates the shared variable flag for calibrating the IMU
#
cFlag = shares.Share()

## @brief Initiates the shared variable for panel flags
#
pFlag = shares.Share(0)

## @brief Initiates the shared variable for panel flags
#
kFlag = shares.Share(0)

## @brief Initiates the shared variable flag for the data collection and file writing
#
dFlag = shares.Share(0)

## @brief Initiates the shared variable flag the haptic feedback from the touch panel calibration
#
hFlag = shares.Share()

## @brief Initiates the shared variable flag for stopping data collection early
#
sFlag = shares.Share()

## @brief Initiates the shared variable for the IMU euler angle data
#
Eang = shares.Share((0, 0, 0))

## @brief Initiates the shared variable for the IMU angular velocity data
#
Vang = shares.Share((0, 0, 0))

## @brief Initiates the shared variable for all of the panel touch data
#
Cart = shares.Share((0, 0, 0, 0, 0))

## @brief Initiates the shared variable for the panel touch xy position data
#
Poxy = shares.Share((0, 0, 0))

## @brief Initiates the shared variable for the panel touch xy velocity data
#
Voxy = shares.Share((0, 0, 0))

## @brief Initiates the shared variable for the motor duty cycle data
#
duty = shares.Share((0, 0))

## @brief Initiates the shared variable for the calibration status data
#
Cast = shares.Share((0, 0, 0, 0))

## @brief Initiates the shared variable for the reference position, velocity and gains for the closed loop controller
#
controlParameters = shares.Share((0, 0, 2.5, 0.2, 0.1))


if __name__ == '__main__':

    ## @brief Stores a list of the MCU functions at the desired frequency to be run simultaneously
    #
    taskList = [taskUserFcn('taskUser', 50_000, wFlag, cFlag, pFlag, kFlag, dFlag, sFlag, duty, Eang, Vang, controlParameters),
                taskMotorFcn('taskMotor', 10_000, wFlag, kFlag, hFlag, cFlag, duty, Eang, Vang, Poxy, Voxy, Cart, Cast, controlParameters),
                taskIMUFcn('taskIMU', 10_100, cFlag, duty, Eang, Vang, Cast),
                taskPanelFcn('taskPanel', 10_000, pFlag, hFlag, Cart, Poxy, Voxy), 
                taskDataFcn('taskData', 20_000, dFlag, sFlag, Eang, Vang, Cart)]

    while True:        # Runs the tasks indefinitely
        try:
            for task in taskList:
                next(task)

        except KeyboardInterrupt:
            break

    print('Program Terminating')