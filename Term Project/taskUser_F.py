"""!@file       taskUser_F.py
    @brief      Runs a user interface for someone to run the encoder
    @details    Uses a function to accept user input and run the desired method.
                Uses the shared variables to read data written by the encoder and motor tasks and to send flags that
                will tell either of the functions to perform a certain action.

                \n The State Transition Diagram for taskUser can be seen here.
                @image html USERFSM.PNG width=1000px
                @image html USERFSMI.PNG width=500px
                @image html USERFSMO.PNG width=500px

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Term%20Project/taskUser_F.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       February 24, 2022
"""

from time import ticks_us, ticks_add, ticks_diff
from pyb import USB_VCP
import micropython

## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Command variable as the constant 1
#
S1_CMD = micropython.const(1)

## @brief Defines the State 2 Print variable as the constant 2
#
S2_POSITION = micropython.const(2)

## @brief Defines the State 3 Velocity variable as the constant 3
#
S3_VELOCITY = micropython.const(3)

## @brief Defines the State 4 Duty Cycle variable as the constant 4
#
S4_INPUT = micropython.const(4)

## @brief Defines the State 5 Reference Velocity variable as the constant 5
#
S5_REF = micropython.const(5)

## @brief Defines the State 6 Set Gain variable as the constant 6
#
S6_SETGAIN = micropython.const(6)

##@brief Defines the State 7 Closed Loop Control variable as the constant 7
#
S7_CLOOP = micropython.const(7)

##@brief Defines the State 8 Calibration variable as the constant 8
#
S8_CAL = micropython.const(8)


def taskUserFcn(taskName, period, wFlag, cFlag, pFlag, kFlag, dFlag, sFlag, duty, Eang, Vang, controlParameters):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name, interval and zFlag status to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param wFlag        Boolean variable shared between taskUser and
                            taskMotor for the closed loop control toggle command
        @param cFlag        Boolean variable shared between taskUser and
                            taskIMU for the IMU calibration command
        @param pFlag        Tuple variable shared between taskUser and
                            taskPanel for the calibration and read commands
        @param kFlag        Tuple variable shared between taskUser and
                            taskMotor for the change gains command
        @param dFlag        Tuple variable shared between taskUser and
                            taskData for the collect data command
        @param sFlag        Boolean variable shared between taskUser and
                            taskData for the stop data collection early command
        @param duty         Tuple shared between taskUser and
                            taskMotor that stores the input duty value
        @param Eang         Tuple shared between taskIMU, taskUser, taskData, and taskMotor
                            that stores euler angle values
        @param Vang         Tuple shared between taskIMU, taskUser, taskData, and taskMotor
                            that stores angular velocity values
        @param controlParameters Tuple shared between taskUser and
                            taskMotor that stores the input reference velocity and proportional gain
    '''
    state = S0_INIT
    next_time = ticks_add(ticks_us(), period)
    ser = USB_VCP()

    # String Instantiation
    buffer = ''
    loop = ''
    
    # Closed Loop Variable Instantiation
    refPos = 0
    refVel = 0
    propGain = 0
    dervGain = 0
    inteGain = 0

    # Counter Instantiation
    itr = 0
    gainCount = 0
    
    # Internal Flag Instantiation
    setPointActive = False
    setGainActive = False
    activeTestChar = False
    
    while True:
       
        current_time = ticks_us()
       
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                printUI()
                state = S1_CMD

            elif state == S1_CMD:      
                if ser.any():
                    charIn = ser.read(1).decode()
                    

                    #--------------Reading Character Inputs----------------    
                    if charIn in {'p', 'P'}:
                        print('\nEuler Angles [deg]')
                        state = S2_POSITION
                        
                    elif charIn in {'v', 'V'}:
                        print('\nAngular Velocity [deg/s]')
                        state = S3_VELOCITY
                        
                    elif charIn in {'s', 'S'}:
                        print('Stopping Data Collection')
                        sFlag.write(True)
                        
                    elif charIn in {'k'}:
                        buffer = ''
                        print('\nChoose Inner Gain Coefficients')
                        print('Choose Proportional Gain')
                        loop = 'in'
                        setGainActive = True
                        state = S4_INPUT
                        
                    elif charIn in {'K'}:
                        buffer = ''
                        print('\nChoose Outer Gain Coefficients')
                        print('Choose Proportional Gain')
                        setGainActive = True
                        state = S4_INPUT
                        
                    elif charIn in {'w', 'W'}:
                        wFlag.write(True)
                        state = S7_CLOOP

                    elif charIn in {'q', 'Q'}:
                        print('\nCalibrating IMU Device')
                        cFlag.write(True)
                        state = S8_CAL
                        
                    elif charIn in {'g', 'G'}:
                        print('\nCalibrating Touch Panel')
                        pFlag.write(3)
                        state = S1_CMD
                        
                    elif charIn in {'d', 'D'}:
                        print('\nChoose Test Article')
                        print('[1] Ball Position\n[2] Ball Velocity\n[3] Panel Angle\n[4] Panel Angular Velocity')
                        activeTestChar = True
                        state = S4_INPUT
                        
                    elif charIn in {'t', 'T'}:
                        itr += 1
                        if itr == 1:
                            print('\nPrinting Touch Panel Contact Position')
                            pFlag.write(4)
                        
                        elif itr == 2:
                            print('\nShutting Off Position Printing')
                            pFlag.write(0)
                            itr = 0
                    
                    elif charIn in {'h', 'H'}:
                        printUI()
                    
                    else:
                        print('Press a valid key \n')
                        printUI()
                        continue


            # --------------- Print Euler Angles ---------------
            elif state == S2_POSITION:
                pitch, roll, yaw = Eang.read()
                print('Pitch, Roll, Yaw')
                print(f'{pitch:.2f}, {roll:.2f}, {yaw:.2f}')
                state = S1_CMD
                    

            # --------------- Print Euler Rates ---------------
            elif state == S3_VELOCITY:
                pitch, roll, yaw = Vang.read()
                print('Pitch Rate, Roll Rate, Yaw Rate')
                print(f'{pitch:.2f}      |   {roll:.2f}   |  {yaw:.2f}')
                state = S1_CMD
                    

            # ---------- Decode Specific User Input -----------
            elif state == S4_INPUT:
                if ser.any():
                    charIn = ser.read(1).decode()
                    

                    # ---- Data Collection Type Switcher ----
                    if activeTestChar == True:
                        if charIn in {'1'}:
                            print('Collecting Ball Position Data')
                            activeTestChar = False
                            dFlag.write(2)
                        elif charIn in {'2'}:
                            print('Collecting Ball Velocity Data')
                            activeTestChar = False
                            dFlag.write(3)
                        elif charIn in {'3'}:
                            print('Collecting Panel Angle Data')
                            activeTestChar = False
                            dFlag.write(4)
                        elif charIn in {'4'}:
                            print('Collecting Panel Angular Velocity Data')
                            activeTestChar = False
                            dFlag.write(5)
                        state = S1_CMD
                    

                    # ---- General Character Drcoding ----
                    elif charIn in {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}:
                        buffer += charIn
                        print(buffer[-1], end='')
                            
                    elif charIn == '-':
                        if len(buffer) == 0:
                            buffer += charIn
                            print(buffer[-1], end='')
                        else:
                            pass
                        
                    elif charIn == '.':
                        if buffer.count('.') == 0:
                            buffer += charIn
                            print(buffer[-1], end='')
                        else:
                            pass
                        
                    elif charIn in {'\b', '\x08', '\x7F'}:
                        if len(buffer) == 0:
                            pass
                        else:
                            buffer = buffer[:-1]
                            print((len(buffer)+1)*'\b' + ' ', end='')
                            print(buffer, end='')
                            
                    elif charIn in {'\r', '\n'}:
                        motorDuty = float(buffer)


                        # Initial Duty Cycle Saturation Limiting
                        if not setGainActive and not setPointActive:

                            if motorDuty > 188.5:
                                motorDuty = 188.5
                            elif motorDuty < -188.5:
                                motorDuty = -188.5
                                

                        # Terminate Input
                        if not setGainActive and not setPointActive:
                            state = S1_CMD
                            

                        # Input to Reference Velocity
                        elif setPointActive == True:
                            state = S5_REF
                            

                        # Input to Gain Coefficient
                        elif setGainActive == True:
                            state = S6_SETGAIN
                            
                        buffer = '\n'     
                            

                    # ---- Escape Input State ----
                    elif charIn in {'s', 'S'}:
                        print('Menu Return')
                        printUI()
                        state = S1_CMD
            

            # --------- Set Closed-Loop Reference Velocity ---------
            elif state == S5_REF:
                print('\nReference Velocity Set')
                refVel = motorDuty
                controlParameters.write((refPos, refVel, propGain, dervGain, inteGain))
                setPointActive = False
                state = S1_CMD


            # --------- Set Closed-Loop Gain Coefficients ----------
            elif state == S6_SETGAIN:
                gainCount += 1

                # ---------- Proportional Gain ----------
                if gainCount == 1:

                    print('\n\nChoose Derivative Gain')
                    propGain = motorDuty
                    controlParameters.write((refPos, refVel, propGain, dervGain, inteGain))
                    state = S4_INPUT

                # ----------- Derivative Gain -----------
                elif gainCount == 2:

                    print('\n\nChoose Integral Gain\nINACTIVE')
                    dervGain = motorDuty
                    controlParameters.write((refPos, refVel, propGain, dervGain, inteGain))
                    #state = S4_INPUT
                    print('\nGains Set')
                    gainCount = 0
                    
                    if loop == 'in':
                        kFlag.write(1)
                    else:
                        kFlag.write(2)
                        
                    loop = ''
                    state = S1_CMD

                    # --------- Integral Gain Currently Inactive ---------
                    '''elif gainCount == 3:
                    # Integral gain
                    #print('\n\nIntegral gain set')
                    #inteGain = motorDuty
                    #controlParameters.write((refPos, refVel, propGain, dervGain, inteGain))
                    print('\nGains Set')
                    gainCount = 0
                    
                    if loop == 'in':
                        kFlag.write(1)
                    else:
                        kFlag.write(2)
                        
                    loop = ''
                    state = S1_CMD'''
                

            # ---------- Toggle Closed Loop Control ----------
            elif state == S7_CLOOP:
                if not wFlag.read():
                    state = S1_CMD


            # ------------ Calibrate Touch Panel -------------
            elif state == S8_CAL:
                if cFlag.read() == False:
                    print('IMU Calibrated\n')
                    state = S1_CMD
            
            next_time = ticks_add(next_time, period)
                
            yield state
       
        else:
            yield None
       
       
def printUI():
    '''!@brief Prints the command screen for the user interface
    '''
    print('+----------------------------------------+\n'
          '|  ME 305 Ball Balancer User Interface   |\n'
          '+----------------------------------------+\n'
          '|              |Commands|                |\n'
          '|p or P   - Print Euler Angles           |\n'
          '|v or V   - Print Angular Velocity       |\n'
          '|k        - Enter Inner Closed Loop Gain |\n'
          '|K        - Enter Outer Closed Loop Gain |\n'
          '|w or W   - Toggle Closed Loop Control   |\n'
          '|t or T   - Show Current Ball Position   |\n'
          '|q or Q   - Calibrate IMU                |\n'
          '|g or G   - Calibrate Touch Panel        |\n'
          '|d or D   - Collect Data of Choice       |\n'
          '|s or S   - End Data Collection Early    |\n'
          '|h or H   - Print This Help Message      |\n'
          '|                                        |\n'
          '|Ctrl + c - Terminate the Program        |\n'
          '+----------------------------------------+')
