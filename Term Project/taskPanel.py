"""!@file       taskPanel.py
    @brief      Runs tasks sent by the taskUser file and keeps the touch panel measurement data updated.
    @details    Acts as a background function file controlling the touch panel for the primary user interface so that
                the user interface does not have to perform touch panel updating directly. It will provide the x and y
                positions and velocities of whatever is touching the touch panel as a shared variable for use in other
                files, and it can calibrate the touch panel when told to by the user task. It will continuously update
                the touch panel every 10 milliseconds when run from main.

                \n The State Transition Diagram for taskIMU can be seen here.
                @image html PANELFSM.PNG width=1000px
                @image html PANELFSMIO.PNG width=500px

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Term%20Project/taskPanel.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       March 17, 2022
"""

from touch import Touch
from pyb import Pin
from time import ticks_diff, ticks_add, ticks_us
import micropython

## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Update variable as the constant 1
#
S1_UPDATE = micropython.const(1)

## @brief Defines the State 2 Set Calibration Coefficients variable as the constant 2
#
S2_SETCOEFF = micropython.const(2)

## @brief Defines the State 3 Get Calibration Coefficients variable as the constant 3
#
S3_GETCOEFF = micropython.const(3)

## @brief Defines the State 4 X-Y Position variable as the constant 4
#
S4_POXY = micropython.const(4)


def taskPanelFcn(taskName, period, pFlag, hFlag, Cart, Poxy, Voxy):
    '''!@brief              Controls the touch panel as determined by the taskUser file
        @details            Interfaces with the taskUser file through the use of
                            the shared flag and tuple parameters, keeping the touch panel
                            updated and performing calibration when told to.
        @param taskName     The name of the task a string
        @param period       The period of the task in microseconds as an integer
        @param pFlag        Tuple variable shared between taskUser and
                            taskPanel for the calibration and read commands
        @param hFlag        Boolean variable shared between taskPanel and
                            taskMotor for the haptic feedback command
        @param Cart         Tuple shared between taskData and
                            taskPanel that contains the touch panel position
                            and velocity data values
        @param Poxy         Tuple shared between taskMotor and taskPanel
                            that stores the touch panel xy position values
        @param Voxy         Tuple shared between taskMotor and taskPanel
                            that stores the touch panel xy velocity values
    '''
    state = S0_INIT
    
    # -- a/b filter Coefficients --
    a = 0.85
    b = 0.005

    # - Panel Offset Instantiation -
    x0 = 0
    y0 = 0

    # ---------- X Pins -----------
    xP  = Pin(Pin.cpu.A7, Pin.IN)
    xM  = Pin(Pin.cpu.A1, Pin.OUT_PP,value=0)
    
    # ---------- Y Pins -----------
    yP = Pin(Pin.cpu.A6, Pin.OUT_PP,value=1)
    yM  = Pin(Pin.cpu.A0, Pin.IN)
    
    # -------- Panel Object -------
    tch = Touch(xP, xM, yP, yM, a, b, x0, y0)

    # -------- Timer Start --------
    next_time = ticks_add(ticks_us(), period)
    
    # --- Counter Instantiation ---
    num = 0
    count = 0
    
    while True:
        current_time = ticks_us()                       # Keeps updating current time
        
        if ticks_diff(current_time, next_time) >= 0:    # Waits for current_time to pass next_time
            
            if state == S0_INIT:
                state = S1_UPDATE
                

            # ---------- State Switcher ----------
            #                  and
            # ---------- Position Update ---------
            elif state == S1_UPDATE:

                if pFlag.read() == 2:
                    state = S2_SETCOEFF
                elif pFlag.read() == 3:
                    state = S3_GETCOEFF
                elif pFlag.read() == 4:
                    state = S4_POXY  

                xp, yp, zs, xv, yv = tch.cart()

                Cart.write((xp, yp, zs, xv, yv))
                Poxy.write((xp, yp, zs))         # Cart Positions
                Voxy.write((xv, yv, zs))         # Cart Velocities
                

            # ---------- Initiates Panel Calibration ----------
            elif state == S2_SETCOEFF:
                if tch.calibrate(hFlag) == True:
                    pFlag.write(3)
                    print('\nCalibration Complete')
                else:
                    state = S1_UPDATE
            

            # ---------- Get Panel Calibration Status ----------
            # --------- Initiates Calibration if False ---------
            elif state == S3_GETCOEFF:
                if tch.getCalCoefficients() == False:
                    state = S3_GETCOEFF
                    count += 1
                    if count == 1:
                        print('Calibration file does not exist\n')
                        print('Touch Center of Panel')
                        pFlag.write(2)
                        state = S1_UPDATE
                        
                else:
                    state = S1_UPDATE
                    pFlag.write(0)
                
                
            # ---------- Print Panel Contact Position ----------
            elif state == S4_POXY:
                state = S1_UPDATE
                if tch.zScan() == True:
                    tempa, tempb, null = Poxy.read()
                    print(tempa, tempb)
                    num = 0
                else:
                    num += 1
                    if num == 1:
                        print('No Contact With Platform')

        
            else:
                raise ValueError(f'Invalid State in {taskName}')
                
            next_time = ticks_add(next_time, period)
            
            yield state
            
        else:
            yield None
        
        