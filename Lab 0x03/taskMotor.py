"""!@file       taskMotor.py
    @brief      Runs tasks sent by the taskUser file.
    @details    Acts as a background function file controlling the motor for the primary user interface
                so that the user interface does not have to perform tasks directly.

                \n The State Transition Diagram for taskUser can be seen here.
                @image html taskMotor_FSM.png width=1000px
                @image html task_I_O_FSM.PNG width=500px
                @image html taskMotor_O.png width=500px

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x03/taskMotor.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       February 16, 2022
"""

from pyb import Pin
from time import ticks_us, ticks_add, ticks_diff
import micropython, driver, closedLoopControl

## @brief Defines the State 0 Initiate variable as the constant 0
#
S0_INIT = micropython.const(0)

## @brief Defines the State 1 Wait variable as the constant 1
#
S1_WAIT = micropython.const(1)

## @brief Defines the State 7 Duty Cycle variable as the constant 7
#
S2_mDUTY = micropython.const(2)

## @brief Defines the State 9 Fulat variable as the constant 9
#
S3_FAULT = micropython.const(3)

##
#
S4_TOGGLE = micropython.const(4)


def taskMotorFcn(taskName, period, mFlag, MFlag, cFlag, wFlag, duty, delta, controlParameters, cLoopFeedback):
    '''!@brief              Performs the tasks as determined by the taskUser file
        @details            Interfaces with the taskUser file through the use of
                            the shared flag parameters and shared values, being able to change the motor duty
                            cycles and clear faults as well as toggling and updating the closed loop controller.
        @param taskName     The name of the task a string
        @param mFlag        Boolean variable shared between taskUser and
                            taskMotor for the motor1 duty cycle command
        @param MFlag        Boolean variable shared between taskUser and
                            taskMotor for the motor2 duty cycle command
        @param cFlag        Boolean variable shared between taskUser and
                            taskMotor for the reset fault command
        @param wFlag        Boolean variable shared between taskUser and
                            taskMotor for the closed loop control toggle command
        @param duty         Tuple shared between taskUser and
                            taskMotor that stores the input duty value
        @param delta        Tuple shared between taskUser and
                            taskEncoder that stores delta data
        @param controlParameters Tuple shared between taskUser and
                            taskMotor that stores the input reference velocity and proportional gain
        @param cLoopFeedback Tuple shared between taskUser and
                            taskMotor that stores the closed loop error feedback from each update
    '''
    ## @brief Begins the function at State 0 before starting the loop
    #
    state = S0_INIT
    
    # Motor 1 pins
    pinB4 = Pin(Pin.cpu.B4, Pin.OUT_PP)
    pinB5 = Pin(Pin.cpu.B5, Pin.OUT_PP)
    
    # Motor 2 pins
    pinB0 = Pin(Pin.cpu.B0, Pin.OUT_PP)
    pinB1 = Pin(Pin.cpu.B1, Pin.OUT_PP)
    
    motorDRV = driver.DRV8847(3)
    motor1 = motorDRV.motor(pinB4, pinB5, 1, 2)
    motor2 = motorDRV.motor(pinB0, pinB1, 3, 4)
    
    # Enable the motor driver
    motorDRV.enable()
    
    ## @brief Stores the value for the next time that the while loop should
    #         be run
    #
    next_time = ticks_add(ticks_us(), period)
    
    cloop = closedLoopControl.ClosedLoop(controlParameters, delta.read())
    
    activeToggle = False
    
    toggle = 0
    
    while True:
        
        current_time = ticks_us()                       # Keeps updating current time
        
        if ticks_diff(current_time, next_time) >= 0:    # Waits for current_time to pass next_time

            if state == S0_INIT:
                state = S1_WAIT
                
                
            elif state == S1_WAIT:
                if mFlag.read():
                    state = S2_mDUTY
                elif MFlag.read():
                    state = S2_mDUTY
                elif cFlag.read():
                    state = S3_FAULT
                elif wFlag.read():
                    
                    toggle += 1
                    if toggle == 1:
                        activeToggle = True
                        print('Closed Loop Control Activated')
                    elif toggle == 2:
                        activeToggle = False
                        print('Closed Loop Control Deactivated')
                        toggle = 0
                        
                delt = delta.read()
                cloop.update(controlParameters, delt)
                cLoopFeedback.write((cloop.motorDuty, cloop.actualVel))
                
                wFlag.write(False)
                
                if activeToggle:
                    DC, actualVel = cLoopFeedback.read()
                    motor1.setDuty(DC)
                
            elif state == S2_mDUTY:
                if mFlag.read():
                    DC, null = duty.read()
                    motor1.setDuty(DC)
                    mFlag.write(False)
                elif MFlag.read(): 
                    null, DC = duty.read()
                    motor2.setDuty(DC)
                    MFlag.write(False)
                state = S1_WAIT
                
            elif state == S3_FAULT:
                motorDRV.enable()
                cFlag.write(False)
                state = S1_WAIT
            
            else:
                raise ValueError(f'Invalid State in {taskName}')
        
            next_time = ticks_add(next_time, period)
            
            yield state
            
        else:
            yield None