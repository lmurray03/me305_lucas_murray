"""!@file       motor.py
    @brief      A driver to change the duty cycle of a connected motor.
    @details    The motor class instantiates a simple motor that will connect to a motor connected to the pin
                and channel arguments. This class allows for the changing of the motor's duty cycle.

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x03/motor.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       February 16, 2022
"""


from pyb import Pin, Timer

class Motor:
    '''!@brief A motor class for one channel of a driver object.
        @details Objects of this class can be used to apply Pulse-Width Modulation (PWM) to a given
                 DC motor, changing the duty cycle.
    '''
    
    def __init__ (self, PWM_tim, IN1_pin, IN2_pin, chA, chB):
        '''!@brief Initializes and returns an object associated with a DC Motor.
            @param PWM_tim  The timer associated with the board motors
            @param IN1_pin  The first pin associated with the motor
            @param IN2_pin  The second pin associated with the motor
            @param chA      The first channel associated with the motor
            @param chB      The second channel associated with the motor
        '''
        
        self.t3chA = PWM_tim.channel(chA, Timer.PWM_INVERTED, pin=IN1_pin)
        self.t3chB = PWM_tim.channel(chB, Timer.PWM_INVERTED, pin=IN2_pin)
    

    # ----------- Set Motor Duty Cycle -----------
    def setDuty (self, duty):
        '''!@brief Set the PWM duty cycle for the motor channel.
            @details This method sets the duty cycle to be sent
                     to the motor to the given level. Positive values
                     cause effort in one direction, negative values
                     in the opposite direction.
            @param duty A signed number holding the duty
                        cycle of the PWM signal sent to the motor.
        '''     
        if duty >= 0:
            self.t3chA.pulse_width_percent(0)
            self.t3chB.pulse_width_percent(duty)
        elif duty < 0:
            duty = -1 * duty
            self.t3chA.pulse_width_percent(duty)
            self.t3chB.pulse_width_percent(0)
    

    # --------------- Stop Motor ---------------
    def killMotor(self):
        '''!@brief A function only used for testing to set motor speed back to zero quickly.
        '''
        self.t3chA.pulse_width_percent(0)
        self.t3chB.pulse_width_percent(0)
        

    # ---------- Panel Haptic Feedback ---------
    def Haptic(self):
        '''!@brief A function only used for momentarily vibrating a motor.
            @details This method alternates the duty cycle of the
                     motor between positive and negative 20 to 
                     generate haptic feedback without causing any
                     net rotation to the motor
        '''
        self.setDuty(20)
        for itr in range (1000):
            itr += 1
        self.setDuty(-20)
        for itr in range (1000):
            itr += 1
        self.setDuty(20)
        for itr in range (1000):
            itr += 1
        self.setDuty(-20)
        for itr in range (1000):
            itr += 1
        self.setDuty(0)

if __name__ == '__main__':
    # Test code for the motor class when run as a standalone program
    PWM_tim = Timer(3, freq = 20_000)
    
    # Motor 1 pins
    pinB4 = Pin(Pin.cpu.B4, Pin.OUT_PP)
    pinB5 = Pin(Pin.cpu.B5, Pin.OUT_PP)
    
    # Motor 2 pins
    pinB0 = Pin(Pin.cpu.B0, Pin.OUT_PP)
    pinB1 = Pin(Pin.cpu.B1, Pin.OUT_PP)
    
    # Motor objects
    motor1 = Motor(PWM_tim, pinB4, pinB5, 1, 2)
    motor2 = Motor(PWM_tim, pinB0, pinB1, 3, 4)
    
    # Enable the motor driver
    nSleep = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
    nSleep.high()
    
    '''
    # Set the duty cycle of the first motor to 40 percent
    motor2.setDuty(-30)
    time.sleep(2)
    motor2.killMotor()
    #motor2.setDuty(25)
    '''